package com.kpi.slava.wishlistapp.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.kpi.slava.wishlistapp.ORM.MovieORM;
import com.kpi.slava.wishlistapp.R;

public class MovieControlDialogFragment extends DialogFragment{

    public static final String TAG = "MovieControlDialogFragment";
    private final int LAYOUT = R.layout.fragment_movie_control;

    private TextInputLayout tilTitle, tilReleaseYear, tilRating;

    private EditText edtTitle, edtReleaseYear, edtRating;

    private Spinner movieGenreSpinner;

    private RadioButton radioUnseen, radioSeen;

    private String[] types = {"<--choose-->", "adventure", "action", "fantasy", "horror"};

    private View view;

    private String title, genre, releaseYear, rating;
    private boolean isSeen = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setTitle("New movie");

        view = inflater.inflate(LAYOUT, container, false);

        radioUnseen = (RadioButton) view.findViewById(R.id.radio_movie_unseen);
        radioUnseen.setChecked(true);
        radioSeen = (RadioButton) view.findViewById(R.id.radio_movie_seen);

        ArrayAdapter<String> typeAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, types);
        typeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        movieGenreSpinner = (Spinner) view.findViewById(R.id.spinner_movie_genre);
        movieGenreSpinner.setAdapter(typeAdapter);

        tilTitle = (TextInputLayout) view.findViewById(R.id.til_enter_movie_title);
        edtTitle = (EditText) tilTitle.findViewById(R.id.edt_enter_movie_title);
        tilTitle.setHint("Enter movie title*");

        tilReleaseYear = (TextInputLayout) view.findViewById(R.id.til_enter_movie_release_year);
        edtReleaseYear = (EditText) tilReleaseYear.findViewById(R.id.edt_enter_movie_release_year);
        tilReleaseYear.setHint("Enter release year");

        tilRating = (TextInputLayout) view.findViewById(R.id.til_enter_movie_rating);
        edtRating = (EditText) tilRating.findViewById(R.id.edt_enter_movie_rating);
        tilRating.setHint("Rate 1..10 *");
        edtRating.setVisibility(View.GONE);

        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group_movie);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radio_movie_unseen) {
                    edtRating.setVisibility(View.GONE);
                    isSeen = false;
                }
                else {
                    edtRating.setVisibility(View.VISIBLE);
                    isSeen = true;
                }
            }
        });

        view.findViewById(R.id.btn_enter_movie_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        view.findViewById(R.id.btn_enter_movie_accept).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get all values for MovieORM-object

                title = edtTitle.getText().toString();

                if(movieGenreSpinner.getSelectedItemPosition() != 0)
                genre = types[movieGenreSpinner.getSelectedItemPosition()];
                else genre = "";

                releaseYear = edtReleaseYear.getText().toString();
                rating = edtRating.getText().toString();

                if(isSeen) {
                    if(checkFieldsIfSeen()){
                        //save movie in db as "seen"

                        new MovieORM(title, genre, releaseYear,true, rating).save();
                        dismiss();
                    } else Toast.makeText(getContext(), "Enter title and rating from 1 to 10", Toast.LENGTH_SHORT).show();
                }
                else{
                    if(title.equals("")) Toast.makeText(getContext(), "Enter title", Toast.LENGTH_SHORT).show();
                    else {
                        // save movie in db as "unseen"

                        new MovieORM(title, genre, releaseYear, false, "").save();
                        dismiss();
                    }
                }
            }
        });

        return view;
    }

    private boolean checkFieldsIfSeen() {

        if((title.equals("")) || (rating.equals("")) || (Double.valueOf(rating) < 1) || (Double.valueOf(rating) > 10)) return false;
        else return true;

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        edtTitle.setText("");
        edtReleaseYear.setText("");
        edtRating.setText("");
        movieGenreSpinner.setSelection(0);
        super.onDismiss(dialog);
    }
}
