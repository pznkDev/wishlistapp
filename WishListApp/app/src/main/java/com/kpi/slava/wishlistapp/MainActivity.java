package com.kpi.slava.wishlistapp;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.kpi.slava.wishlistapp.ORM.MovieORM;
import com.kpi.slava.wishlistapp.fragments.BooksFragment;
import com.kpi.slava.wishlistapp.fragments.ContentStorageFragment;
import com.kpi.slava.wishlistapp.fragments.HomeFragment;
import com.kpi.slava.wishlistapp.fragments.MovieControlDialogFragment;
import com.kpi.slava.wishlistapp.fragments.MoviesFragment;
import com.kpi.slava.wishlistapp.fragments.NotesFragment;

import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Toolbar toolbar;

    private HomeFragment homeFragment;
    private MoviesFragment moviesFragment;
    private BooksFragment booksFragment;
    private ContentStorageFragment contentStorageFragment;
    private NotesFragment notesFragment;

    private MovieControlDialogFragment movieControlDialogFragment;

    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    private com.getbase.floatingactionbutton.FloatingActionButton fab_add_movie, fab_add_book, fab_add_note;

    private final int CONTAINER = R.id.container;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToolbar();
        initNavigationDrawer();
        initFragments();

        initFabs();

        List<MovieORM> movies = MovieORM.listAll(MovieORM.class);
        for(int i=0; i<movies.size(); i++){
            System.out.println("title = " + movies.get(i).getTitle() + '\n' +
                                "genre = " + movies.get(i).getGenre() + '\n' +
                                "releaseYear = " + movies.get(i).getReleaseYear() + '\n' +
                                "date = " + movies.get(i).getDate() + '\n' +
                                "isSeen = " + movies.get(i).isSeen() + '\n' +
                                "rating = " + movies.get(i).getRating());
        }

    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initNavigationDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void initFragments() {

        homeFragment = new HomeFragment();
        moviesFragment = new MoviesFragment();
        booksFragment = new BooksFragment();
        contentStorageFragment = new ContentStorageFragment();
        notesFragment = new NotesFragment();

        movieControlDialogFragment = new MovieControlDialogFragment();

        fragmentManager = getSupportFragmentManager();
        transaction = fragmentManager.beginTransaction();

        transaction.add(CONTAINER, homeFragment).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        transaction = fragmentManager.beginTransaction();

        switch(item.getItemId()){
            case (R.id.nav_home) :

                transaction.replace(CONTAINER, homeFragment);

                break;

            case (R.id.nav_movies) :

                transaction.replace(CONTAINER, moviesFragment);

                break;

            case (R.id.nav_books) :

                transaction.replace(CONTAINER, booksFragment);

                break;

            case (R.id.nav_my_list) :

                transaction.replace(CONTAINER, contentStorageFragment);

                break;

            case (R.id.nav_notes) :

                transaction.replace(CONTAINER, notesFragment);

                break;

            case (R.id.nav_settings) :

                // dialogFragment for settings

                break;

            case (R.id.nav_close) :
                finish();
                break;
        }

        transaction.commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void initFabs() {
        findViewById(R.id.fab_add_movie).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                movieControlDialogFragment.show(getSupportFragmentManager(), MovieControlDialogFragment.TAG);
            }
        });

        findViewById(R.id.fab_add_book).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "add book", Toast.LENGTH_SHORT).show();
            }
        });

        fab_add_note = (com.getbase.floatingactionbutton.FloatingActionButton) findViewById(R.id.fab_add_note);
        fab_add_note.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "add note", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
