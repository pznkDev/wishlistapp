package com.kpi.slava.wishlistapp.ORM;

import com.orm.SugarRecord;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MovieORM extends SugarRecord {

    String title,genre, releaseYear, date, rating;
    boolean isSeen;

    public MovieORM() {
    }

    public MovieORM(String title, String genre, String releaseYear, boolean isSeen, String rating) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy ");
        date = dateFormat.format(new Date());
        this.isSeen = isSeen;
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public String getGenre() {
        return genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public String getDate() {
        return date;
    }

    public String getRating() {
        return rating;
    }

    public boolean isSeen() {
        return isSeen;
    }
}
