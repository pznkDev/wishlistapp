package com.kpi.slava.wishlistapp.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kpi.slava.wishlistapp.R;

/**
 * Created by Slava on 23.05.2016.
 */
public class NotesFragment extends Fragment {

    public static final String TAG = "NotesFragment";
    private final int LAYOUT = R.layout.fragment_notes;

    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(LAYOUT, container, false);

        return view;
    }

}
